var renderer, scene, camera;
var textures = [];

// DOF parameters
var focus = 500, aperture = 0.0001, maxblur = 1.0;

function loadTextures() {
    var loader = new THREE.TextureLoader();

    function loadTex(image_index) {
        var image_url = "imgs/textures/tex" + image_index.toString() + ".png";
        loader.load(
            // resource URL
            image_url,
            // Function when resource is loaded
            function( texture ) {
                textures.push(texture);
                loadTex(image_index + 1);
            }
        );
    }
    
    loadTex(0);
}

function initWebGL() {
    container = document.createElement( 'div' );
    container.className = "scene scene-full";
    var navbar = document.getElementById("navbar");
    document.body.insertBefore( container, navbar.nextSibling);

    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setPixelRatio((window.devicePixelRatio) ? window.devicePixelRatio : 1);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.autoClear = false;
    renderer.setClearColor(0x000000, 0.0);
    container.appendChild( renderer.domElement );

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 3000);
    camera.position.z = 1;
    scene.add(camera);

    loadTextures();

    var ambientLight = new THREE.AmbientLight(0x999999 );
    scene.add(ambientLight);
  
    window.addEventListener('resize', onWebGLResize, false);
};

function onWebGLResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

// ANIMATION
// =========

var bkg_objects = [];

function createObject() {
    // Object
    var length = 200 + Math.random() * 640 / 3;
    var tex = Math.floor(Math.random() * textures.length);
    var geometry = new THREE.PlaneGeometry(length, length);
    // var material = new THREE.MeshBasicMaterial({map:textures[tex]});
    var material = new THREE.ShaderMaterial({
        uniforms: {
            tColor: {
                value: textures[tex]
            },
            maxblur: {
                value: maxblur
            },
            aperture: {
                value: aperture
            },
            tofocus: {
                value: 0.0
            },
            aspect: {
                value: 1.0
            },
            opacity: {
                value: 1.0
            }
        },
        vertexShader: document.getElementById("wrapVertexShader").textContent,
        fragmentShader: document.getElementById("wrapFragmentShader").textContent,
        transparent:true
    });
    material.transparent = true;
    plane = new THREE.Mesh(geometry, material);

    // Initial position
    var x = -400 + Math.random() * 800;
    var y = (-400 + Math.random() * 800) * window.innerHeight / window.innerWidth;
    var z = -250 - Math.random() * 150;
    plane.position.x = x;
    plane.position.y = y;
    plane.position.z = z;

    // Speed
    var vx = -2 + Math.random() * 4;
    var vy = -2 + Math.random() * 4;
    var vz = -1 - Math.random() * 3;

    // Fade in/live/fade out
    var fade_in = 15 + Math.random() * 30;
    var fade_out = 30 + Math.random() * 60;
    var live_min = 1.1 * (focus + z) / -vz;
    var live_max = 2.0 * live_min;
    var live = live_min + Math.random() * (live_max - live_min);

    var object = {
        plane:plane,
        vx:vx,
        vy:vy,
        vz:vz,
        fade_in:fade_in,
        live:live,
        fade_out:fade_out,
        frame:0,
        frame_max:fade_in + live + fade_out,
        animate: function() {
            this.plane.position.x += vx;
            this.plane.position.y += vy;
            this.plane.position.z += vz;
            this.frame += 1;
            if (this.frame > this.frame_max) {
                this.plane.material.opacity = 0.0;
            }
            else if (this.frame > this.fade_in + this.live) {
                var f = (this.frame - (this.fade_in + this.live));
                this.plane.material.opacity = 1.0 - f / this.fade_out;
            }
            else if (this.frame > this.fade_in) {
                this.plane.material.opacity = 1.0;
            }
            else {
                this.plane.material.opacity = this.frame / this.fade_in;
            }
            // Renew the uniform values
            this.plane.material.uniforms.maxblur.value = maxblur;
            this.plane.material.uniforms.aperture.value = aperture;
            this.plane.material.uniforms.tofocus.value = focus + this.plane.position.z;  // Plane position is already negative
            this.plane.material.uniforms.aspect.value = 1.0;
            this.plane.material.uniforms.opacity.value = this.plane.material.opacity;
        }
    };

    bkg_objects.push(object)
    scene.add( plane );
    return object;
}

function animateWebGL() {
    requestAnimationFrame(animateWebGL);

    var gen_obj_prob = 0.01;
    if ( ( textures.length > 0 ) && ( Math.random() < gen_obj_prob ) ) {
        createObject();
    }

    for (var i=bkg_objects.length - 1; i >= 0; i--){
        bkg_objects[i].animate();
        if (bkg_objects[i].frame > bkg_objects[i].frame_max) {
            scene.remove( bkg_objects[i].plane );
            bkg_objects.splice(i, 1);
        }
    }
    
    renderer.clear();
    renderer.render(scene, camera);
}
