var JSON_DATA = undefined;

function hoverTimeline(d, i, datum) {
    // d is the current rendering object
    // i is the index during d3 rendering
    // datum is the id object
    var div = $('#hoverRes');
    div.find('#toolName').html(datum.label);
    snapshot = JSON_DATA["snapshot"];
    const sample = snapshot["samples"][i];
    const t0 = Math.round(d.starting_time / 1000);
    const t1 = Math.round(d.ending_time / 1000);
    const dt = t1 - t0;
    div.find('#toolTimers').html(
        t0 + ' &#181;s' + ' &rarr; ' + t1 + ' &#181;s' +
        ' (' + dt + ' &#181;s)'
    );

    const container = div.find('#toolDeps');
    var deps_txt = "";
    for (const depname of sample["dependencies"]["in"]) {
        deps_txt = deps_txt + '&#x1F4E5; ' + depname + '<br>';
    }
    for (const depname of sample["dependencies"]["out"]) {
        deps_txt = deps_txt + '&#x1F4E4; ' + depname + '<br>';
    }
    div.find('#toolDeps').html(deps_txt);
}

function makeTimeline(data) {
    JSON_DATA = data;

    var labelData = [];
    snapshot = data.snapshot;
    for (const sample of snapshot.samples) {
        var obj = {label: sample.name, times: [] };
        for (let i = 0; i < sample.start.length; i++) {
            obj.times.push({starting_time: sample.start[i],
                            ending_time: sample.end[i]})
        }
        labelData.push(obj);
    }

    var width = "100%";
    const formatNumber = d3.format(",.0f"); 
    const formatNanoSeconds = d => `${formatNumber(d)} ns`;
    var colorScale = d3.scaleOrdinal().range(['#ef9b0f','#0f9900']);
    function timeline() {
        var chart = d3.timelines()
            .width(width*4)
            .stack()
            .relativeTime()
            .tickFormat({format: formatNanoSeconds, tickTime: d3.timeMilliseconds})
            .colors( colorScale )
            .margin({left:240, right:100, top:0, bottom:0})
            .hover(hoverTimeline)
            .click(function (d, i, datum) {
                // alert(datum.label);
            });

        var svg = d3.select("#timeline3").append("svg").attr("width", width)
            .datum(labelData).call(chart);
    }

    timeline();

    // Resize the svg
    var div = $('#timeline3');
    var svg = div.find('svg');
    // Get the bounds of the SVG content
    var  bbox = svg[0].getBBox();
    // Update the width and height using the size of the contents
    svg[0].setAttribute("height", bbox.y + bbox.height + bbox.y);
}


window.onload = function() {
    load(undefined);
}
