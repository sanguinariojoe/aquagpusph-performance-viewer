function groupByName(data) {
    names = {};
    new_data = {
        elapsed: data.elapsed,
        progress: data.progress,
        snapshot: { samples: [], step: data.step }
    };
    for (const sample of data.snapshot.samples) {
        if (sample.name in names) {
            const i = names[sample.name];
            new_data.snapshot.samples[i].start.push(sample.start);
            new_data.snapshot.samples[i].end.push(sample.end);
        } else {
            names[sample.name] = new_data.snapshot.samples.length;
            new_data.snapshot.samples.push({
                name: sample.name,
                dependencies: sample.dependencies,
                start: [sample.start],
                end: [sample.end],
            });
        }
    }
    return new_data;
}

function loadFile(fileObj, onFinish) {
    assert(fileObj.name.endsWith(".json"), "Not a JSON file")
    var reader = new FileReader();
    reader.onload = function() {
        const json_data = groupByName(JSON.parse(reader.result));
        
        makeTimeline(json_data);
    };
    reader.readAsText(fileObj);
}

function load(evt) {
    // Open a file selection dialog
    var fileDiag = $('<input type="file" style="display:none;" accept=".json">');
    var reader;
    fileDiag.trigger('click');
    // Read the selected file
    fileDiag.on('change', function(e) {
        loadFile(e.target.files[0]);
    });
}
