function assert(bool, text) {
    if (bool) {
        return;
    }
    throw text;
}
